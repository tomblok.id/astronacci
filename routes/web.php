<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::namespace('App\Http\Controllers')->group(function () {
    Auth::routes();
});

Route::get('/', function () {
    return redirect()->intended('home');
});

Route::get('/home', [
    App\Http\Controllers\HomeController::class,
    'index',
])->name('home');

Route::get(
    'auth/social',
    'App\Http\Controllers\Auth\LoginController@show'
)->name('social.login');

Route::get(
    'oauth/{driver}',
    'App\Http\Controllers\Auth\LoginController@redirectToProvider'
)->name('social.oauth');

Route::get(
    'oauth/{driver}/callback',
    'App\Http\Controllers\Auth\LoginController@handleProviderCallback'
)->name('social.callback');

Route::post(
    '/join-member',
    'App\Http\Controllers\HomeController@joinMember'
)->name('join-member');
