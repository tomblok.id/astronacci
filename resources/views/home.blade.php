@extends('layouts.app')

@section('content')
<div class="container py-4">
    <div class="row justify-content-center">
        @if (Auth::user()->level == null)
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12" style="cursor:pointer" onclick="event.preventDefault(); document.getElementById('join-member-silver').submit();">
                <div class="card" style="margin-top:5px">
                    <div class="card-header text-center">Member Silver</div>

                    <div class="card-body text-center">
                        3 artikel dan 3 video bebas diakses
                    </div>
                </div>
                <form id="join-member-silver" action="{{ route('join-member') }}" method="POST" class="d-none">
                    <input type="hidden" value="1" name="level">
                    @csrf
                </form>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12" style="cursor:pointer" onclick="event.preventDefault(); document.getElementById('join-member-gold').submit();">
                <div class="card" style="margin-top:5px">
                    <div class="card-header bg-warning text-center text-white">Member Gold</div>

                    <div class="card-body text-center">
                        10 artikel dan 10 video bebas diakses
                    </div>
                </div>
                <form id="join-member-gold" action="{{ route('join-member') }}" method="POST" class="d-none">
                    <input type="hidden" value="2" name="level">
                    @csrf
                </form>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12" style="cursor:pointer" onclick="event.preventDefault(); document.getElementById('join-member-platinum').submit();">
                <div class="card" style="margin-top:5px">
                    <div class="card-header text-center bg-primary text-white">Member Platinum</div>
                    <div class="card-body text-center">
                        Semua artikel dan video bebas diakses
                    </div>
                </div>
                <form id="join-member-platinum" action="{{ route('join-member') }}" method="POST" class="d-none">
                    <input type="hidden" value="3" name="level">
                    @csrf
                </form>
            </div>
        </div>
        @else
        
            
        <div class="card">
            <div class="card-body">
            <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="card" style="margin-top:5px">
                    @if ($level == 1)
                    <div class="card-header bg-default text-center">Silver</div>
                    @elseif ($level == 2)
                    <div class="card-header bg-warning text-white text-center">Gold</div>
                    @else
                    <div class="card-header bg-primary text-white text-center">Platinum</div>
                    @endif
                    <div class="card-body">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-artikel-tab" data-toggle="tab" href="#v-artikel" role="tab" aria-controls="v-artikel" aria-selected="true">Artikel</a>
                            <a class="nav-link" id="v-video-tab" data-toggle="tab" href="#v-video" role="tab" aria-controls="v-video" aria-selected="false">Video</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-6 col-xs-12" >
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="v-artikel" role="tabpanel" aria-labelledby="v-artikel-tab">
                        @include('content.artikel')
                    </div>
                    <div class="tab-pane fade" id="v-video" role="tabpanel" aria-labelledby="v-video-tab">
                        @include('content.video')
                    </div>
                </div>
            </div>
            <div>
            <div>
        </div>
        @endif
    </div>
</div>
@endsection


