
    <div class="row">
        @for ($i = 0; $i < count($video); $i++)
        <div class="col-md-12 col-sm-6 col-xs-12" >
            <div class="card" style="margin-top:5px">
                <div class="card-header"><h3>{{$video[$i]->title}}</h3></div>
                    <div class="card-body">
                        <div style="display:inline;">
                            <ul class="list-inline" style="display:inline;" >
                                <li class="list-inline-item"><i class="bi bi-person-square"></i></i> Posting by {{$video[$i]->by}}</li>
                            </ul>
                        </div>
                        <div class = "media">
                            <iframe src="{{$video[$i]->link}}" title="description" width="100%" height="500px"></iframe>
                        </div>
                            <!-- <p style="text-align:right;">
                                <button class="btn btn-primary">Read More</button>
                            </p> -->
                    </div>
                </div>
            </div>
        @endfor
    </div>