
    <div class="row">
        @for ($i = 0; $i < count($artikel); $i++)
        <div class="col-md-12 col-sm-6 col-xs-12" >
            <div class="card" style="margin-top:5px">
                <div class="card-header"><h3>{{$artikel[$i]->title}}</h3></div>
                    <div class="card-body">
                        <div style="display:inline;">
                            <ul class="list-inline" style="display:inline;" >
                                <li class="list-inline-item"><i class="bi bi-clock-fill"></i></i> {{$artikel[$i]->date}} </li>
                                <li class="list-inline-item"><i class="bi bi-eye-fill"></i></i> {{$artikel[$i]->view}}</li>
                                <li class="list-inline-item"><i class="bi bi-star-fill"></i></i> {{$artikel[$i]->like}}</li>
                                <li class="list-inline-item"><i class="bi bi-person-square"></i></i> Posting by {{$artikel[$i]->by}}</li>
                                <li class="pull-right"> Category : {{$artikel[$i]->categori}}</li>
                            </ul>
                        </div>
                        <div class = "media">
                            <img class = "media-object" src = "{{$artikel[$i]->img}}" width="100%" height="100%">
                        </div>
                        <div class = "media-body">
                            <p>
                                {{$artikel[$i]->content}}
                            </p> 
                        </div>
                    </div>
                </div>
            </div>
        @endfor
    </div>