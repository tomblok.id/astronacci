<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtikelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artikel', function (Blueprint $table) {
            $table->id();
            $table->text('title')->nullable();
            $table->string('date')->nullable();
            $table->string('view')->nullable();
            $table->string('like')->nullable();
            $table->string('by')->nullable();
            $table->string('category')->nullable();
            $table->text('content')->nullable();
            $table->text('img')->nullable();
            $table->timestamps();
        });

        $data = [
            [
                'title' =>
                    '1. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '2. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '3. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '4. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '5. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '6. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '7. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],

            [
                'title' =>
                    '8. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '9. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '10. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '11. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '12. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '13. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '14. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
            [
                'title' =>
                    '15. The standard Lorem Ipsum passage, used since the 1500s',
                'date' => 'Jan 5, 2016',
                'view' => '21K',
                'like' => '372',
                'by' => 'Ilmu Detil',
                'category' => 'Education',
                'content' =>
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img' =>
                    'https://creativecrafts.space/wp-content/uploads/sites/9/2019/06/FREE.jpg',
            ],
        ];

        for ($i = 0; $i < count($data); $i++) {
            DB::table('artikel')->insert($data[$i]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artikel');
    }
}
