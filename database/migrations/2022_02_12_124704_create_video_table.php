<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video', function (Blueprint $table) {
            $table->id();
            $table->text('title')->nullable();
            $table->string('by')->nullable();
            $table->text('link')->nullable();
            $table->timestamps();
        });

        $data = [
            [
                'title' => '1. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '2. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '3. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '4. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '5. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '6. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '7. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '8. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '9. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '10. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '11. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '12. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '13. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '14. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
            [
                'title' => '15. translation by H. Rackham',
                'by' => 'Lorem Ipsum',
                'link' => 'https://www.youtube.com/embed/StMAC8ngrPs',
            ],
        ];

        for ($i = 0; $i < count($data); $i++) {
            DB::table('video')->insert($data[$i]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video');
    }
}
