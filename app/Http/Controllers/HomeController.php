<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Artikel;
use App\Models\Video;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->level == 1) {
            $artikel = Artikel::limit(3)->get();
            $video = Video::limit(3)->get();
        } elseif ($user->level == 2) {
            $artikel = Artikel::limit(10)->get();
            $video = Video::limit(10)->get();
        } else {
            $artikel = Artikel::get();
            $video = Video::get();
        }
        return view('home', [
            'level' => $user->level,
            'artikel' => $artikel,
            'video' => $video,
        ]);
    }

    public function joinMember(Request $request)
    {
        $update = Auth::user();
        $update->level = $request->level;
        $update->save();

        return redirect()->intended('home');
    }
}
